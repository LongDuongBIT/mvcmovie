﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Buoi1.Startup))]
namespace Buoi1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
