namespace Buoi2_Ban2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAnnotations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "Rate", c => c.Double(nullable: false));
            AlterColumn("dbo.Movies", "Genre", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Movies", "Genre", c => c.String());
            DropColumn("dbo.Movies", "Rate");
        }
    }
}
