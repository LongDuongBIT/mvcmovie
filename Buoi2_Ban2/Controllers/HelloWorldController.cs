﻿using System.Web.Mvc;

namespace Buoi2_Ban2.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: HelloWorld
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Welcome(string name, string numTimes)
        {
            ViewBag.Name = "Hello " + name;
            ViewBag.NumTime = numTimes;
            // ViewBag.WelcomeMessage = string.Format("Welcome {0}! NumTime is {1}", name, numTimes);
            return View();
        }
    }
}