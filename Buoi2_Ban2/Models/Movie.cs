﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Buoi2_Ban2.Models
{
    public class Movie
    {
        public int ID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string Title { get; set; }

        [Display(Name = "Ngày phát hành")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }

        [StringLength(10, ErrorMessage = "sdjkfhsdkfhsdjk")]
        public string Genre { get; set; }

        public double Rate { get; set; }
        public decimal Price { get; set; }
    }
}