﻿using System.Data.Entity;

namespace Buoi2_Ban2.Models
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext() : base("DefaultConnection")
        {
        }

        public DbSet<Movie> Movies { get; set; }
    }
}